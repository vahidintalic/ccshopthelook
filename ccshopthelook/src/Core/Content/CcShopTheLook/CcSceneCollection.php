<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(CcSceneCollection $entity)
 * @method void              set(string $key, CcSceneCollection $entity)
 * @method CcSceneCollection[]    getIterator()
 * @method CcSceneCollection[]    getElements()
 * @method CcSceneCollection|null get(string $key)
 * @method CcSceneCollection|null first()
 * @method CcSceneCollection|null last()
 */
class CcSceneCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return CcSceneEntity::class;
    }
}
