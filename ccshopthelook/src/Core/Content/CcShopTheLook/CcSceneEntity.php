<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CcSceneEntity extends Entity
{
    use EntityIdTrait;


    /**
     *  @var string
     */
    protected $img;

    /**
     *
     * @return string
     */
    public function getImg(): string
    {
        return $this->img;
    }

    /**
     *
     * @param string $name
     */
    public function setImg(string $img): void
    {
        $this->img = $img;
    }


}
