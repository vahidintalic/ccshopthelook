<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneCategory;

use ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcCategory\CcCategoryDefinition;
use ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\Country\CountryDefinition;

class CcSceneCategoryDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'cc_scene_category';
    }

    public function getCollectionClass(): string
    {
        return CcSceneCategoryCollection::class;
    }

    public function getEntityClass(): string
    {
        return CcSceneCategoryEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        /*
         * IdField Id
         * StringField category_name
         * FkField category_id
         * FkField scene_id
         * //ManyToOneAssociation country to CountryDefinition
         *
         * required: scene_id
         */

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new StringField('category_name', 'category_name'))->addFlags(new Required()),

                new FkField('scene_id', 'scene_id', CcSceneDefinition::class),
                new ManyToOneAssociationField(
                    'scene_id',
                    'scene_id',
                    CcSceneDefinition::class,
                    'id',
                    false
                ),

                new FkField('category_id', 'category_id', CcCategoryDefinition::class),
                new ManyToOneAssociationField(
                    'category_id',
                    'category_id',
                    CcCategoryDefinition::class,
                    'id',
                    false
                )

            ]
        );
    }
}
