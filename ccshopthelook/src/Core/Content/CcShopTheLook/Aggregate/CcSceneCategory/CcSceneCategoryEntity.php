<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneCategory;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Country\CountryEntity;

class CcSceneCategoryEntity extends Entity
{
    use EntityIdTrait;


    /**
     *  @var string|null
     */
    protected $category_name;

    /**
     *  @var integer|null
     */
    protected $category_id;

    /**
     *  @var integer
     */
    protected $scene_id;


    /**
     *
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->category_name;
    }

    /**
     *
     * @param string $category_name
     */
    public function setCategoryName(string $category_name): void
    {
        $this->category_name = $category_name;
    }

}
