<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(CcSceneCategoryCollection $entity)
 * @method void              set(string $key, CcSceneCategoryCollection $entity)
 * @method CcSceneCategoryCollection[]    getIterator()
 * @method CcSceneCategoryCollection[]    getElements()
 * @method CcSceneCategoryCollection|null get(string $key)
 * @method CcSceneCategoryCollection|null first()
 * @method CcSceneCategoryCollection|null last()
 */
class CcSceneCategoryCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return CcSceneCategoryEntity::class;
    }
}
