<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcCategory;

use ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneCollection;
use ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcSceneEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CcCategoryDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'cc_category';
    }

    public function getCollectionClass(): string
    {
        return CcCategoryCollection::class;
    }

    public function getEntityClass(): string
    {
        return CcCategoryEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        /*
         * IdField Id
         * StringField img
         * // ManyToOneAssociation country to CountryDefinition
         *
         * required: img
         */

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new StringField('img', 'img'))->addFlags(new Required()),

            ]
        );
    }
}
