<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(CcCategoryCollection $entity)
 * @method void              set(string $key, CcCategoryCollection $entity)
 * @method CcCategoryCollection[]    getIterator()
 * @method CcCategoryCollection[]    getElements()
 * @method CcCategoryCollection|null get(string $key)
 * @method CcCategoryCollection|null first()
 * @method CcCategoryCollection|null last()
 */
class CcCategoryCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return CcCategoryEntity::class;
    }
}
