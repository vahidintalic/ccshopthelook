<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate\CcCategory;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Country\CountryEntity;

class CcCategoryEntity extends Entity
{
    use EntityIdTrait;


    /**
     *  @var string
     */
    protected $img;

    /**
     *
     * @return string
     */
    public function getImg(): string
    {
        return $this->img;
    }

    /**
     *
     * @param string $img
     */
    public function setImg(string $img): void
    {
        $this->img = $img;
    }
}
