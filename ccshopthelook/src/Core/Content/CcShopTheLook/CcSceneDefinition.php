<?php declare(strict_types=1);

namespace ccshopthelook\Core\Content\CcShopTheLook\Aggregate;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use UsingRefs\Product;

class CcSceneDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'cc_scene';
    }

    public function getCollectionClass(): string
    {
        return CcSceneCollection::class;
    }

    public function getEntityClass(): string
    {
        return CcSceneEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        /*
         * IdField Id
         * FkField product_id
         * FkField scene_id
         * StringField position_right
         * StringField position_top
         *
         *
         * required: img
         */

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new StringField('position_right', 'position_right'))->addFlags(new Required()),
                (new StringField('position_right', 'position_right'))->addFlags(new Required()),

                new FkField('product_id', 'product_id', Product::class),
                new ManyToOneAssociationField(
                    'product_id',
                    'product_id',
                    Product::class,
                    'id',
                    false
                ),

                new FkField('scene_id', 'scene_id', CcSceneDefinition::class),
                new ManyToOneAssociationField(
                    'scene_id',
                    'scene_id',
                    CcSceneDefinition::class,
                    'id',
                    false
                )

            ]
        );
    }
}
