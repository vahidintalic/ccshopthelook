import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-pointer',
    label: 'Image boxed pointer123',
    category: 'text-image',
    component: 'sw-cms-block-image-pointer',
    previewComponent: 'sw-cms-preview-image-pointer',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        center: 'image'
    }
});
