import { Component } from 'src/core/shopware';
import template from './sw-cms-preview-image-right-reversed.html.twig';
import './sw-cms-preview-image-right-reversed.scss';

Component.register('sw-cms-preview-image-right-reversed', {
    template
});
