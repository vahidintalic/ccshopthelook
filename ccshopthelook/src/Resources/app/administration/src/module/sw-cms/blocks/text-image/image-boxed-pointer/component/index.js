import { Component } from 'src/core/shopware';
import template from './sw-cms-block-image-pointer.html.twig';
import './sw-cms-block-image-pointer.scss';

Component.register('sw-cms-block-image-left-reversed', {
    template
});
