import { Component } from 'src/core/shopware';
import template from './sw-cms-preview-image-pointer.html.twig';
import './sw-cms-preview-image-pointer.scss';

Component.register('sw-cms-preview-image-pointer', {
    template
});
