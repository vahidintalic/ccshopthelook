import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-right-reversed',
    label: 'Right side image pointer',
    category: 'text-image',
    component: 'sw-cms-block-image-right-reversed',
    previewComponent: 'sw-cms-preview-image-right-reversed',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        right: 'image',
        left: 'text'
    }
});
