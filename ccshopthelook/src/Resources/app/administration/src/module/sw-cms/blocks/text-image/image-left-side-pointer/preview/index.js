import { Component } from 'src/core/shopware';
import template from './sw-cms-preview-image-left-reversed.html.twig';
import './sw-cms-preview-image-left-reversed.scss';

Component.register('sw-cms-preview-image-left-reversed', {
    template
});
