import { Component } from 'src/core/shopware';
import template from './sw-cms-block-image-right-reserved.html.twig';
import './sw-cms-block-image-right-reversed.scss';

Component.register('sw-cms-block-image-right-reversed', {
    template
});
