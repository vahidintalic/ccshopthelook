import { Component } from 'src/core/shopware';
import template from './sw-cms-block-image-full-sized-pointer.html.twig';
import './sw-cms-block-image-full-sized-pointer.scss';

Component.register('sw-cms-block-image-full-sized-pointer', {
    template
});
