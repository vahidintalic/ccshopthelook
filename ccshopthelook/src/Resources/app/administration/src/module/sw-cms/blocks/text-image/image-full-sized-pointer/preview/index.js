import { Component } from 'src/core/shopware';
import template from './sw-cms-preview-image-full-sized-pointer.html.twig';
import './sw-cms-preview-image-full-sized-pointer.scss';

Component.register('sw-cms-preview-image-full-sized-pointer', {
    template
});
