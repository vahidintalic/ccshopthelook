import { Component } from 'src/core/shopware';
import template from './sw-cms-block-image-left-reserved.html.twig';
import './sw-cms-block-image-left-reversed.scss';

Component.register('sw-cms-block-image-left-reversed', {
    template
});
