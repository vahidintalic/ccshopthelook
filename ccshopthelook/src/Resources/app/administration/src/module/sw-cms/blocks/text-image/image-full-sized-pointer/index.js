import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-full-sized-pointer',
    label: 'Image full sized pointer',
    category: 'text-image',
    component: 'sw-cms-block-image-full-sized-pointer',
    previewComponent: 'sw-cms-preview-image-full-sized-pointer',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        center: 'image'
    }
});
