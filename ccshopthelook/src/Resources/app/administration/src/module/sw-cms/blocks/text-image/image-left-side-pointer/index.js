import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-left-reversed',
    label: 'Left side image pointer',
    category: 'text-image',
    component: 'sw-cms-block-image-left-reversed',
    previewComponent: 'sw-cms-preview-image-left-reversed',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        right: 'image',
        left: 'text'
    }
});
