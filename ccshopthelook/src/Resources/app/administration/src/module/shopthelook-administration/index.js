const { Module } = Shopware;

Module.register('shopthelook-administration', {
    type: 'plugin',
    name: 'shopthelook-administration',
    title: 'Shop The Look',
    color: '#ff3d58',
    icon: 'default-shopping-paper-bag-product',
    description: 'Configure your Shop The Look plugin here',

    routes: {
        list: {
            component: 'list_options',
            path: 'shop-the-look',
        }
    },
    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    navigation: [{
        id: 'shopthelook',
        label: 'Shop the look',
        color: '#6fad23',
        path: 'shop-the-look',
        icon: 'default-shopping-paper-bag-product',
        position: 20
    }]
});
